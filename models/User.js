const bcrypt = require('bcrypt-nodejs');
const crypto = require('crypto');
const mongoose = require('mongoose');

const medicoesSchema = mongoose.Schema({
  valAssin: { type: String, trim: true },
  idCurva: { type: String, trim: true },
  chaveP: { type: String, trim: true },
  validade: { type: String, trim: true },
  umidade: { type: String, trim: true },
  assinatura: { type: String, trim: true },
  data: { type: String, trim: true },
  nomeGrao: { type: String, trim: true },
  idCalib: { type: String, trim: true }
});

const equipSchema = mongoose.Schema({
  nserie: {
    type: String, required: true, trim: true, unique: true
  },
  block: { type: Boolean, required: true, default: false },
  cnpj: { type: String, required: true, trim: true },
  medicoes: [medicoesSchema],
  data_cad: { type: Date, default: Date.now }
});

const userSchema = new mongoose.Schema({
  email: { type: String, unique: true },
  password: String,
  passwordResetToken: String,
  passwordResetExpires: Date,

  facebook: String,
  twitter: String,
  google: String,
  github: String,
  instagram: String,
  linkedin: String,
  steam: String,
  tokens: Array,
  equipamentos: [equipSchema],

  profile: {
    name: String,
    gender: String,
    location: String,
    website: String,
    picture: String,
    permission: { type: String, default: 'user' },
  }
}, { timestamps: true });

/**
 * Password hash middleware.
 */
userSchema.pre('save', function save(next) {
  const user = this;
  if (!user.isModified('password')) { return next(); }
  bcrypt.genSalt(10, (err, salt) => {
    if (err) { return next(err); }
    bcrypt.hash(user.password, salt, null, (err, hash) => {
      if (err) { return next(err); }
      user.password = hash;
      next();
    });
  });
});

/**
 * Helper method for validating user's password.
 */
userSchema.methods.comparePassword = function comparePassword(candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
    cb(err, isMatch);
  });
};

/**
 * Helper method for getting user's gravatar.
 */
userSchema.methods.gravatar = function gravatar(size) {
  if (!size) {
    size = 200;
  }
  if (!this.email) {
    return `https://gravatar.com/avatar/?s=${size}&d=retro`;
  }
  const md5 = crypto.createHash('md5').update(this.email).digest('hex');
  return `https://gravatar.com/avatar/${md5}?s=${size}&d=retro`;
};

const User = mongoose.model('User', userSchema);

module.exports = User;
