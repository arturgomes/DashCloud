exports.admin = function admin(req, res, next) {
  if (req.user) {
    if (req.user.permission === 'admin') {
      return next();
    }
  }
  return res.redirect(req.header('Referer') || '/');
};
