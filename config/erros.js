exports.notfound = function notfound(req, res) {
  res.status(404);
  res.render('not-found', {
    rota: req.path
  });
};

exports.serverError = function serverError(err, req, res) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
};
